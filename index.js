// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoutes.js")

// Server setup
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect("mongodb+srv://admin:admin1234@b256-dizon.choclmo.mongodb.net/B256_to-do?retryWrites=true&w=majority",

									{
										useNewUrlParser:true,
										useUnifiedTopology:true
								}

				);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}))

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/task", taskRoute)

// Server Listening
app.listen(port, () => console.log(`Server is running at port ${port}`));

